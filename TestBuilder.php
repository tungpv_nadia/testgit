<?php
interface BuilderInterface{
	public function build();
	public function getTitle();
	public function getColor();
} 
	/**
	* Product
	*/
	class Product 
	{
		private $title;
		private $color;
	}

	/**
	* Director
	*/
	class Director{
		private $product;
		public function __construct(BuilderInterface $builder){
			$this->product = new Product();
			$this->product->title = $builder->getTitle();
			$this->product->color = $builder->getColor();
		}
		public function getResult(){
			return $this->product;
		}
	}
	/**
	* 
	*/
	class BuilderA implements BuilderInterface
	{
		private $title;
		private $color;

		public function setTitle($title){
			$this->title = $title;
			return $title;
		}
		public function setColor($color){
			$this->color = $color;
			return $color;
		}

		public function getTitle(){
			return $this->title;
		}

		public function getColor(){
			return $this->color;
		}
		public function buil(){
			return new Director($this);
		}
	}
	class BuilderB implements BuilderInterface
	{
		private $title;
		private $color;

		public function setTitle($title){
			$this->title = $title;
			return $title;
		}
		public function setColor($color){
			$this->color = $color;
			return $color;
		}

		public function getTitle(){
			return $this->title;
		}

		public function getColor(){
			return $this->color;
		}
		public function buil(){
			return new Director($this);
		}
	}

	$builder = new BuilderA();
	$builder->setTitle('example');
	$result = $builder->buil();
echo $result;

	?>